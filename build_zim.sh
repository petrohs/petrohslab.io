#!/bin/bash

zim --index xonakatl/notebook.zim

[ -e public/ ] && rm -rf public/

zim --export \
    --format=html --template=GitLabPagesTemplate \
    --output=./public \
    --overwrite \
    --verbose \
  xonakatl/notebook.zim

