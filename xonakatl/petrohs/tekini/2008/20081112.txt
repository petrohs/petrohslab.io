Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2019-02-10T22:44:43-06:00

====== Empleo del teclado en fluxbox ======
20081112

De las cosas que me agradan en [[http://fluxbox.org/|fluxbox]] es el empleo de combinaciones en teclado para ejecutar acciones de una manera rápida y fácil de configurar.

Se requiere modificar el archivo //.fluxbox/keys// con las atajos y su respectiva función. Para no desperdiciar la tecla de //winbug$// hago uso de esta como //Mod4//; para identificar como reconoce esta tecla empleo el comando //xev//.
	'''
	petrohs@tlahtolli:/home/petrohs$ cat .fluxbox/keys
	OnDesktop Mouse1 :HideMenus
	OnDesktop Mouse2 :WorkspaceMenu
	OnDesktop Mouse3 :RootMenu
	OnDesktop Mouse4 :NextWorkspace
	OnDesktop Mouse5 :PrevWorkspace
	Mod1 Tab :NextWindow
	Mod1 Shift Tab :PrevWindow
	Mod4 F1 :Workspace 1
	Mod4 F2 :Workspace 2
	Mod4 F3 :Workspace 3
	Mod4 F4 :Workspace 4
	Mod4 F5 :Workspace 5
	Mod4 F6 :Workspace 6
	Mod4 F7 :Workspace 7
	Mod4 F8 :Workspace 8
	Mod4 F9 :Workspace 9
	Mod4 F10 :Workspace 10
	Mod4 F11 :Workspace 11
	Mod4 F12 :Workspace 12
	Mod4 l :ExecCommand xlock
	Mod4 t :ExecCommand xterm
	Mod4 f :ExecCommand firefox
	Mod4 r :ExecCommand bbrun -w
	Mod4 g :ExecCommand gvim
	Mod4 m :ShowDesktop
	XF86AudioMute :ExecCommand mocp --pause
	Mod4 XF86AudioMute :ExecCommand mocp --unpause
	XF86AudioRaiseVolume :ExecCommand mocp -v +2
	XF86AudioLowerVolume :ExecCommand mocp -v -2
	Mod4 Menu :RootMenu
	Mod4 3 :Maximize
	Mod4 Shift 3 :Minimize
	Mod4 comma :ExecCommand gksudo dhclient
	Mod4 period :ExecCommand gksudo "/sbin/iwconfig wlan0 essid red key contra"
	petrohs@tlahtolli:/home/petrohs$
	'''


Donde:
	''Mod1'' Alt
	''Mod4'' Win
	''NextWindow'' 	Siguiente ventana
	''PrevWindow'' 	Ventana anterior
	''Workspace #'' 	Cambia al escritorio #
	''ExecCommand'' 	Ejecuta el comando
	''ShowDesktop'' 	Minimiza todo
	''RootMenu'' 	Muestra el menú
	''Maximize'' 	Maximizar ventana
	''Minimize'' 	Minimizar ventana 
