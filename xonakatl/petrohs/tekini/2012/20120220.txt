Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-12T23:46:31-05:00

====== vi :Terminal too wide ======
20120220

Al ejecutar vi en un servidor remoto obtengo el error
	'':Terminal too wide''

Para solucionarlo ejecutar en consola
	''$ stty columns 180''

