Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-12T23:28:04-05:00

====== generar secuencias ======
20120309

Para obtener la cadena correspondiente a una secuencia que va de 0-Z
'''
$ cat sec.bash
#!/bin/bash
sec="S0000000A9Z";
ad=1;
for s in `seq -w 02 11 | sort -rn`
  do
    c=`expr substr $sec $s 1`; cc=`printf '%d' "'$c"`;
    e=`expr $cc +  $ad`; ad=0; 
    if [ $e -eq 58 ]; then e=65; fi;
    if [ $e -eq 91 ]; then e=48; ad=1; fi;
    nsec="$(printf "\\$(printf '%03o' $e)")${nsec}"
  done
echo -e " $sec \n S$nsec";
$ ./sec.bash
 S0000000A9Z
 S0000000AA0
'''

