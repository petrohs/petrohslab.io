Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-12T23:06:54-05:00

====== mocp error ======
20120810

Al intentar ejecutar mocp falla con el error
''mocp: interface_elements.c:3891: iface_set_mixer_value: La declaración `value >= 0 && value <= 100' no se cumple. Abortado''
{{./pasted_image.png}}

Recordé que usando [[http://library.gnome.org/users/gnome-volume-control/stable/|gnome-volume-control]] había ajustado el volumen al máximo (150%).
{{./pasted_image001.png}}

Basta con regresar a 100% y mocp vuelve a funcionar :D 
