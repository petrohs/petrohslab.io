Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-12T22:27:11-05:00

====== Coloreado de sintaxis en formato wiki para vim ======
20150908

En ocasiones editamos algún artículo de [[http://es.wikipedia.org/|Wikipedia]] desconectados (cosas raras de wikimaniacos). Si empleamos como editor [[http://www.vim.org/|Vim]], por omisión, no colorea las etiquetas wiki; sin embargo, existe el complemento [[http://www.vim.org/scripts/script.php?script_id=1787|wikipedia.vim]] que resuelve este inconveniente.

Vamos a nuestro directorio de vim (si no existe algun directorio lo creamos)
Para colorear se emplea:
'''
$ cd ~/.vim/syntax/
$ wget 'https://raw.githubusercontent.com/chikamichi/mediawiki.vim/master/syntax/mediawiki.vim'
'''


En vim emplearíamos para llamar
	'':setf mediawiki''

Para que vim detecte automáticamente
'''
$ cd ~/.vim/ftdetect/
$ wget 'https://raw.githubusercontent.com/chikamichi/mediawiki.vim/master/ftdetect/mediawiki.vim'
'''
 
{{./pasted_image.png}}

Para que gvim muestre la opción en el menú
'''
$ cd ~/.vim/
$ vi synmenu.vim
:cal SetSyn("mediawiki")
'''

{{./pasted_image001.png}}

Tambien se puede hacer estas instalaciones por [[https://github.com/chikamichi/mediawiki.vim|git]]. Esta información se detalla en la propia [[https://en.wikipedia.org/wiki/Wikipedia%3AText_editor_support#Vim|wikipedia]].
