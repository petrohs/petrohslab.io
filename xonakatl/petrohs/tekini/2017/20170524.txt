Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2017-05-24T16:46:03-05:00

====== Uso básico de screen ======
20170524 

Screen es una utilería de GNU Linux que permite crear multisesiones en la consola, ya sea dividiendo la terminal en varias consolas o permitir a múltiples consolas conectarse a la misma sesión.

===== Uso básico =====

==== Listar ====
Al ingresar a un servidor Linux listamos la lista de sesiones abiertas
{{{code: lang="sh" linenumbers="True"
$ ssh dbadmin@10.103.53,10
[dbadmin@mxmgclnd1 ~]$ screen -ls
There are screens on:
		155109.vbr_01   (Detached)
		329631.vbr_backup       (Detached)
2 Sockets in /var/run/screen/S-dbadmin.
[dbadmin@mxmgclnd1 ~]$
}}}

{{./pasted_image.png}}

==== Conectar ====
En este caso observamos dos sesiones presentes, nos conectaremos a la sesión llamada vbr_backup
{{{code: lang="sh" linenumbers="True"
[dbadmin@mxmgclnd1 ~]$ screen -x vbr_backup
}}}

{{./pasted_image001.png}}

La ventana cambiara a lo que se encuentre en ese momento la sesión. 
Conectando con el mismo usuario, se pueden incorporar a la misma sesión cualquier cantidad de veces y en todas las simultaneas se vera la misma pantalla y cualquiera de ellas puede controlar la consola. 

==== Desconectar ====
Para desconectarse de la sesión sin matarla con lo que las demás permanecerán presionar la combinación
ctrl+a +d
{{./pasted_image002.png}}
Y regresaremos al punto donde se veían las sesiones

==== Nueva ====
Si se quiere iniciar una nueva sesión se emplea 
[dbadmin@mxmgclnd1 ~]$ screen -S ejemplo
{{./pasted_image003.png}}

==== Cerrar ====
Para terminar una sesión, no importa cuantos estén conectados es con 
{{{code: lang="sh" linenumbers="True"
ctrl+a +k
}}}

Y pedirá confirmación 

{{{code: lang="sh" linenumbers="True"
ctrl+d
}}}

Matara la sesión sin pedir confirmación 

===== Uso intermedio =====

==== Ventanas ====
Se puede tener dentro de una misma sesión mas de una ventana. Como si se conectaran mas veces al servidor pero desde la misma pantalla 
{{{code: lang="sh" linenumbers="True"
ctrl+a +c
}}}


==== Lista ====
Para listar las ventanas creadas se usa
{{{code: lang="sh" linenumbers="True"
ctrl+a +" 
}}}

{{./pasted_image004.png}}

==== Moverse ====
Para ir recorriendo las ventanas se usa la combinación
{{{code: lang="sh" linenumbers="True"

ctrl+a +n 
ctrl+a +p
}}}


==== Dividir ====
Se puede dividir la ventana en horizontal
{{{code: lang="sh" linenumbers="True"
ctrl+a +S
}}}


Se puede dividir la ventana en vertical (pero no todas las versiones lo soportan)
{{{code: lang="sh" linenumbers="True"
ctrl+a +|
}}}


Para cambiar a la división
{{{code: lang="sh" linenumbers="True"
ctrl+a  +tab
}}}


{{./pasted_image005.png}}

Para subir el buffer
{{{code: lang="sh" linenumbers="True"
ctrl+a +esc  (subir o bajar con las flechas) 
enter (para salir de ese modo)
}}}


Para ampliar la terminal al maximizar la ventana
{{{code: lang="sh" linenumbers="True"
ctrl+a +F
}}}


==== Redimencionar ====
Ajustar el tamaño nuevamente de la pantalla de screen
{{{code: lang="sh" linenumbers="True"
ctrl+a +F
}}}
