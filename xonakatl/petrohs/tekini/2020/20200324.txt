Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-13T01:02:02-05:00

====== Leer la salida de un comando desde vim ======
20200324

Dentro de vim se puede ejecutar un comando escribiendo
	''<esc>:!comando''
{{./pasted_image.png}}

Esto da como resultado
{{./pasted_image001.png}}


Pero si esta salida la queremos directamente dentro de vim se puede emplear
	''<esc>:read !comando''
{{./pasted_image002.png}}

Con lo que obtenemos
{{./pasted_image003.png}}

