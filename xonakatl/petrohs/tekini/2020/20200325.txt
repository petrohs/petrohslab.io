Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-13T01:05:14-05:00

====== Cambiar color de la barra de estado en vim ======
20200325

Cuando dividimos la ventana en vim se puede cambiar el color de la //barra de estado//.
{{./pasted_image.png}} {{./pasted_image001.png}}

Con eso se usa, respectivamente en //gvim// y //vim//
'''
<esc>:highlight StatusLine guibg=red guifg=yellow
<esc>:highlight StatusLine ctermbg=red ctermfg=yellow
'''
{{./pasted_image002.png}} {{./pasted_image003.png}}

Ahora, para cambiar la barra de la sección que no esta en uso.
'''
<esc>:highlight StatusLineNC guibg=magenta guifg=cyan
<esc>:highlight StatusLineNC ctermbg=magenta ctermfg=cyan
'''
{{./pasted_image004.png}} {{./pasted_image005.png}}



