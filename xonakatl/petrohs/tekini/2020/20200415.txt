Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-17T23:00:18-05:00

====== seq --format ======
20200415

Para generar un control de secuencias se tiene el numero inicial, el numero final y la cantidad de caracteres.
Por ejemplo queremos del 7 al 11, pero necesitamos que sea de cuatro cifras. Para ello usamos la [[https://www.gnu.org/software/coreutils/manual/html_node/seq-invocation.html|opción]] -f y como se completa.

{{{code: lang="sh" linenumbers="False"
$ seq 7 11
7
8
9
10
11
$ seq -w 7 11
07
08
09
10
11
$ seq -f "%03g" 7 11    
0007
0008
0009
0010
0011
}}}

{{./pasted_image.png}}
