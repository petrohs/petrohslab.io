Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-04-17T16:42:04-05:00

====== dwdiff ======
20200416

Se tienen dos archivos muy parecidos
{{./pasted_image001.png}}

Usando `diff` se comparan las líneas distintas
{{{code: lang="sh" linenumbers="False"
diff d.1 d.2
}}}

{{./pasted_image002.png}}

Pero no se aprecian los valores distintos.

Leí sobre la [[https://os.ghalkes.nl/dwdiff.html|dwdiff]], el cual trabaja sobre palabras (o campos si indicamos el separador) en lugar de líneas.
{{{code: lang="sh" linenumbers="False"
dwdiff d.1 d.2
}}}

{{./pasted_image003.png}}

Ahora podemos ver las diferencias entre [-arch1-] y {+arch2+}
{{{code: lang="sh" linenumbers="False"
23X [-B2Y-] {+BZY+} 
 X [-MOR-] {+M0R+}
 23 [-HIH-] {+H1H+}
 YWE [-UID-] {+ULD+}
 
}}}


O usar color, con rojo el primer archivo y en verde el dato del segundo archivo
{{{code: lang="sh" linenumbers="False"
dwdiff --color d.1 d.2
}}}

{{./pasted_image004.png}}


