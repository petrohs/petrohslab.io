Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-06-16T23:58:21-05:00

====== Esquema de colores personalizado en vim ======
20200616

Me interesaba crear una lista de tareas en vim pero que tenga un esquema de colores a mi gusto. 

Para ello se usan dos sentencias en vim: 
- syntax match
- hi 

* La primera se emplea para crear una expresión regular y asignarla a una etiqueta
	''syntax match OHScTarea /^· /''

* La segunda se emplea para indicar como colorear la etiqueta
	''hi petrohsScTarea guifg=green''

{{./pasted_image.png}}
Usando las siguientes configuraciones
	'''
	syntax match petrohScTarea /^· /
	hi petrohScTarea guifg=green
	
	syntax match petrohScRepet /^÷ /
	hi petrohScRepet guifg=blue
	
	syntax match petrohScWR /^× / 
	hi petrohScWR guifg=yellow
	
	syntax match petrohScProy /^º /
	hi petrohScProy guifg=red
	
	syntax match petrohScCompleta /\[[øØ]\]/
	hi petrohScCompleta guifg=green
	
	syntax match petrohScNoCompleta /\[[xX]\]/
	hi petrohScNoCompleta guifg=red
	
	syntax match petrohScCancelada /\[[cC]\]/
	hi petrohScCancelada guifg=yellow
	
	syntax match petrohScOtraCompleta /\[[^øØcCxX]\]/
	hi petrohScOtraCompleta guifg=gray
	
	syntax match petrohScPers /[æÆ]\w\+/
	hi def link petrohScPers Type
	
	syntax match petrohScTareaNom /«.*»/
	hi petrohScTareaNom gui=italic guifg=cyan
	
	syntax match petrohScFechaInicio /ŧ\d\{8}/
	hi petrohScFechaInicio guifg=SeaGreen
	
	syntax match petrohScFechaFin /Ŧ\d\{8}/
	hi petrohScFechaFin guifg=DarkYellow
	
	syntax match petrohScTareaPuntoD /Ð/
	hi def link petrohScTareaPuntoD PreProc
	
	syntax match petrohScTareaPunto /°/
	hi petrohScTareaPunto guifg=Orange
	
	syntax match petrohScFechaOtra /\d\{4}\/\d\{2}\/\d\{2}/
	hi petrohScFechaOtra guifg=lightblue
	
	syntax match petrohScPrioridad /¶\w\+/
	hi def link petrohScPrioridad Special
	
	syntax match petrohScFinBloque /,,,/
	hi petrohScFinBloque guifg=gray
	
	syntax match petrohScEtiqWR /ħ\w\+/
	hi petrohScEtiqWR guifg=white
	
	syntax match petrohScEtiqRFC /rfc\d\{4}C\d\{4}/
	hi petrohScEtiqRFC guifg=lightblue
	
	syntax match petrohScEtiqTipoNota /[¢µ@ł]/
	hi petrohScEtiqTipoNota guifg=lightblue
	
	syntax match petrohScPrioridadProy /Ω/
	hi def link petrohScPrioridadProy Special
	
	syntax match petrohScEtiqCont /\w\+ŋ/
	hi petrohScEtiqCont guifg=white
	
	syntax match petrohScTareaRepet /®\w\+/
	hi petrohScTareaRepet gui=italic
	'''


