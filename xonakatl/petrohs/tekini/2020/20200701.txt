Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2020-09-13T22:59:42-05:00

====== entr ======

Para hacer tener un monitor de un conjunto de archivos esta [[https://github.com/eradman/entr/|entr]] 

Por ejemplo al tener una serie de logs que crecen si tiene alguna nueva notificación

'''
term1$ cat bitacora.log
ERROR 1
term1$

term2$ ls -1 bitacora.log | entr echo cambio
cambio
term2$

term1$ echo "ERROR 2" >> bitacora.log
term1$
'''

{{./pasted_image.png}}

En el ejemplo fue con un simple echo pero puede ser un comando [[http://eradman.com/entrproject/|complejo]], un tar, una notificación u otro script
